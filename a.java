import Controller from '@ember/controller';
import emberUtils from 'ciderepository/utils/emberutils';
import ajaxUtils from "ciderepository/utils/common/ajaxutils";
import commonUtils from "ciderepository/utils/common/commonutils";
import repoUtils from "ciderepository/utils/common/repoutils";
import repoInitUtils from 'ciderepository/utils/common/repoinitutils';
import repoInitUtilsMixin from 'ciderepository/utils/common/repoinitutils_mixin';

import commitInfoMixin from 'ciderepository/mixins/commit_info_mixin';
import validateConstraint from "ciderepository/utils/validation/validation_constraints";

export default Controller.extend(commitInfoMixin, {
        
    appCtrl: emberUtils.injectController('application'),
    
    thisCtrl: 'files/file_edit',
    
    loadController: function(branchName, filePath) {
        let self = this,
            fileName = commonUtils.getLastSlashValue(filePath);
        self.get('appCtrl').repoSideBarItemActive("nav_files");
        self.setProperties({
            exitEdit: false,
            branchName: branchName,
            filePath: filePath,
            fileName: fileName,
            editFileViewType: "file_edit",
            newFilePath: commonUtils.removeLastSlashValue(filePath),
            committer: {},
	        commitTime: null,
	        commitMessage: repoUtils.getCommitMessage("FILE", "EDIT").replace("{{fileName}}", fileName),
	        showErrorMessage: false,
	        isUpdating: false,
            isRepoFileContentLoading: false,
            isRepoFileContentDiffLoading: false,
            fileContent: '',
            editedFileContent: '',
            diffViews: [{
                name: 'Side by Side',
                key: 'side-by-side',
                svgIcon: 'side',
                active: true
            }, {
                name: 'Inline',
                key: 'line-by-line',
                svgIcon: 'inline',
                active: false
            }],
            diffPortionFileContent: '',
            isFileContentEditFullView: false,
            wrapLines: true
        });
        self.getFileContent(branchName, filePath);
        self.getLastestCommit(branchName, filePath);
    },
    
    getFileContent: function(branchName, filePath) {
        let self = this,
            repoFilesReq = { 
                url: 'filecontent?filePath='+ encodeURIComponent(filePath) + '&revision=' + encodeURIComponent(branchName)
            };
        self.set('isRepoFileContentLoading', true);
    	ajaxUtils.GET(repoFilesReq, (fileContentRes) => {
    	    self.set('isRepoFileContentLoading', false);
    	    let fileContent = fileContentRes.fileContent;
    	    if(!emberUtils.isNone(fileContent)) {
                let fileContentVal = fileContent.content;
                self.setProperties({
    	            'fileContent': fileContentVal,
                    'editedFileContent': fileContentVal
    	        });
    	    } else {
    	        commonUtils.serverErrorResponse({ errObj: fileContentRes.errObj, showTooltip: true });
    	    }
    	});
    },

    getFileEditDiff: function() {
        let self = this,
            editedFileContent = self.get('editedFileContent'),
            diffPortionFileContent = self.get('diffPortionFileContent');
        if(editedFileContent === diffPortionFileContent) { 
            return;
        }
        let fileEditDiffReqPayLoad = {
                content: editedFileContent,
                name: self.get("filePath"),
                changeset: self.get('latestCommit').changeset
            },
            fileEditDiffReq = {
                url: 'stage/diff',
                data: JSON.stringify(fileEditDiffReqPayLoad)
            };
        self.set("isRepoFileContentDiffLoading", true);
        ajaxUtils.POST(fileEditDiffReq, (fileEditDiffRes) => {
            if(ajaxUtils.isSuccess(fileEditDiffRes)) {
                let fileEditDiffResDiff = fileEditDiffRes.diff;
                self.setProperties({
                    "editFileViewDiff": fileEditDiffResDiff,
                    "diffPortionFileContent": editedFileContent
                });

            } else {
                commonUtils.serverErrorResponse({ errObj: fileEditDiffRes.errObj, thisCtrl: self});
            }
            self.set("isRepoFileContentDiffLoading", false);
        });
    },
    
    actions: {

        getFileContentMoreMenuItems: function() {
	        let self = this,
	            fileMoreOptions = [];
	        fileMoreOptions.push({
	                name: 'Wrap Lines',
	                action: 'toggleWrapLines',
	                selected: self.get('wrapLines')
	            });
    		return fileMoreOptions;
    	},
                
        gotoResourcePath: function(filepath, isFolder) {
            let self = this,
                branchName = self.get('branchName');
            if(isFolder) {
                self.transitionToRoute('files/tree', branchName, filepath);
            }
            else {
                self.transitionToRoute('files/file_content', branchName, filepath);
            }
        },
                
        goBackToFiles: function(newFilePath = null) {
            let self = this,
                filePath = self.get('filePath'),
                branchName = self.get('branchName');
            if(!emberUtils.isNone(newFilePath) && newFilePath.length > 0) {
                filePath = newFilePath;
            }
            self.set("exitEdit", true);
            self.transitionToRoute('files/file_content', branchName, filePath);
        },

        toggleWrapLines: function() {
            let self = this;
            self.set('wrapLines', !self.get('wrapLines'));
        },

        toggleEditFileViewType: function(viewType) {
            let self = this;
            self.set("editFileViewType", viewType);
            if(viewType === "file_edit_diff") {
                self.getFileEditDiff();
            }
        },

        toggleFileContentEditFullView: function() {
            let self = this;
            self.set("isFileContentEditFullView", !self.get("isFileContentEditFullView"));
        },

        onEditFileContentChange: function(editorObj) {
            this.set("editedFileContent", editorObj.getValue());
        },

        /**
         * Change diff view mode.
         */
        changeDiffView: function(key) {
            let self = this,
                diffViews = self.get('diffViews');
            diffViews.setEach('active', false);
            emberUtils.set(diffViews.findBy('key', key), 'active', true);
            repoInitUtils.changeDiffView(key);
        },
        
        updateFileContent: function() {
            let self = this,
                validationRes = self.validation();
            if(!validationRes) {
                return;
            }
            let fileContent = self.get('editedFileContent'),
                branchName = self.get('branchName'),
                fileName = self.get("fileName"),
                oldFilePath = self.get("filePath"),
                filePath = self.get('newFilePath') + "/" + fileName,
                message = self.get('commitMessage'),
                fileEditPayload = {
                    "content": fileContent,
                    "revision": branchName,
                    "oldFilePath": oldFilePath,
                    "name": filePath,
                    "message": message,
                    "changeset": self.get('latestCommit').changeset
                },
				fileEditReq = { url: "files", data: JSON.stringify(fileEditPayload) };
			self.set("showErrorMessage", false);
			self.set("isUpdating", true);
			ajaxUtils.PUT(fileEditReq, (fileEditRes) => {
			    self.set("isUpdating", false);
			    if(ajaxUtils.isSuccess(fileEditRes)) {
			        self.set("exitEdit", true);
			        commonUtils.showFormSuccessMessage("File content updated");
			        self.transitionToRoute('files/file_content', branchName, filePath);
			    }
			    else {
			        self.set("showErrorMessage", true);
			        if(fileEditRes.errObj.code === 12303) {
			            self.set("errorMsg", fileEditRes.errObj.reason);
			        } else {
			            commonUtils.serverErrorResponse({ errObj: fileEditRes.errObj, thisCtrl: self});
			        }
			    }
			});
        }
    },
    
    validation: function() {
        let self = this,
	        validationObj = self.getValidationFieldObj(),
	        validateObj = validateConstraint.getvalidateObj(self, validationObj),
	        validateSuccess = validateConstraint.validateResultObj(cideValidate(validateObj.values, validateObj.constraints, {format: "detailed"}), { inline: true, validationObj: validationObj });
        return validateSuccess;
    },
    
    getValidationFieldObj: function() {
        let validationFieldObj = {
            'fileName': { validate: 'fileName'},
            'commitMessage': { validate: 'commitMessage', showErrLabel: true}
         };
        return validationFieldObj;
	}
});